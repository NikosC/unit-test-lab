import jdk.internal.loader.AbstractClassLoaderValue;

import java.util.Scanner;
public class SimpleCalculator {

    public static int Addition(int a, int b){
        if(a>255 || a<-255 || b>255 || b<-255) {
            throw new IllegalArgumentException("Out of range! (-255, 255)");
        }
        return a + b;
    }

    public static int Subtraction(int a, int b){
        return a - b;
    }

    public static int Multiplication(int a, int b){
        return a * b;
    }

    public static double Division(double a, double b){
        if(b==0){
            throw new ArithmeticException("Cannot divide by zero!");
        }
        return a / b;
    }

    public static int[] additionList(int[] a, int[] b){
        if(a.length != b.length){
            throw new ArrayIndexOutOfBoundsException("Arrays are different sizes!");
        }
        int[] r;
        r = new int[a.length];

        for(int i=0; i<a.length; ++i){
            r[i] = Addition(a[i], b[i]);
        }
        return r;
    }

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter first number: ");
        int a = reader.nextInt();
        System.out.println("Enter function: ");
        String f = reader.next();
        System.out.println("Enter second number: ");
        int b = reader.nextInt();
        reader.close();

        if(f.equals("+")){
            System.out.println("Result is: ");
            System.out.println(Addition(a, b));
        }
        else if(f.equals("-")){
            System.out.println("Result is: ");
            System.out.println(Subtraction(a, b));
        }
        else if(f.equals("X") || f.equals("x") || f.equals("*")){
            System.out.println("Result is: ");
            System.out.println(Multiplication(a, b));
        }
        else if(f.equals("/")){
            System.out.println("Result is: ");
            System.out.println(Division((double) a, (double) b));
        }
        else{
            System.out.println("Invalid function\n");
        }

        int[] aa;
        int[] ab;
        aa = new int[2];
        ab = new int[3];
        aa[0] = 1;
        aa[1] = 2;
        ab[0] = 1;
        ab[1] = 2;
        ab[2] = 3;
        additionList(aa, ab);
    }

}


