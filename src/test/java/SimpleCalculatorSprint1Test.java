import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class SimpleCalculatorSprint1Test {

    SimpleCalculator calculator;

    @Before
    public void setUp() throws Exception {
        calculator = new SimpleCalculator();
    }

    @After
    public void tearDown() throws Exception {
    }



    @Test
    public void UT_Addition_pos() {
        assertThat("Addition does not work! (2 pos)", calculator.Addition(1, 2), is(3));
    }
    @Test
    public void UT_Addition_neg(){
        assertThat("Addition does not work! (2 neg)", calculator.Addition(-1, -2), is(-3));
    }
    @Test
    public void UT_Addition_negAndPos(){
        assertThat("Addition does not work! (pos & neg)", calculator.Addition(-1, 2), is(1));
    }



    @Test
    public void UT_Subtraction_pos() {
        assertThat("Subtraction does not work! (2 pos)", calculator.Subtraction(1, 2), is(-1));
    }
    @Test
    public void UT_Subtraction_neg() {
        assertThat("Subtraction does not work! (2 neg)", calculator.Subtraction(-1, -2), is(1));
    }
    @Test
    public void UT_Subtraction_negAndPos() {
        assertThat("Subtraction does not work! (pos & neg)", calculator.Subtraction(-1, 2), is(-3));
    }



    @Test
    public void UT_Multiplication_pos() {
        assertThat("Multiplication does not work! (2 pos)", calculator.Multiplication(2, 2), is(4));
    }
    @Test
    public void UT_Multiplication_neg() {
        assertThat("Multiplication does not work! (2 neg)", calculator.Multiplication(-2, -2), is(4));
    }
    @Test
    public void UT_Multiplication_negAndPos() {
        assertThat("Multiplication does not work! (pos & neg)", calculator.Multiplication(-2, 2), is(-4));
    }



    @Test
    public void UT_Division_pos() {
        assertThat("Division does not work! (2 pos)", calculator.Division(1, 2), is(0.5));
    }
    @Test
    public void UT_Division_neg() {
        assertThat("Division does not work! (2 neg)", calculator.Division(-1, -2), is(0.5));
    }
    @Test
    public void UT_Division_negAndPos() {
        assertThat("Division does not work! (pos & neg)", calculator.Division(-1, 2), is(-0.5));
    }

}