import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SimpleCalculatorTest2 {

    SimpleCalculator calculator;

    @Before
    public void setUp() throws Exception {
        calculator = new SimpleCalculator();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test(expected = IllegalArgumentException.class)
    public void UT_Addition_range() {
        calculator.Addition(260,260);
    }
    @Test(expected = ArithmeticException.class)
    public void UT_Divide_zero() {
        calculator.Division(1,0);
    }
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void UT_ArrayList_oob() {
        int[] aa;
        int[] ab;
        aa = new int[2];
        ab = new int[3];
        aa[0] = 1;
        aa[1] = 2;
        ab[0] = 1;
        ab[1] = 2;
        ab[2] = 3;
        calculator.additionList(aa, ab);
    }
}