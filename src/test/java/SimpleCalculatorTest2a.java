import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SimpleCalculatorTest2a {

    SimpleCalculator calculator;

    @Before
    public void setUp() throws Exception {
        calculator = new SimpleCalculator();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {{ -1, 1, 0, -2, -1, -1 }, { -2, -1, -3, -1, 2, 2 }, { 6, 3, 9, 3, 18, 2 }});
    }

    @Parameterized.Parameter
    public int a;

    @Parameterized.Parameter(1)
    public int b;

    @Parameterized.Parameter(2)
    public int ra;
    @Test
    public void UT_Addition_p() {
        assertThat("Addition does not work!", calculator.Addition(a, b), is(ra));
    }


    @Parameterized.Parameter(3)
    public int rs;
    @Test
    public void UT_Subtraction_p() {
        assertThat("Subtraction does not work!", calculator.Subtraction(a, b), is(rs));
    }

    @Parameterized.Parameter(4)
    public int rm;
    @Test
    public void UT_Multiplication_p() {
        assertThat("Multiplication does not work!", calculator.Multiplication(a, b), is(rm));
    }

    @Parameterized.Parameter(5)
    public int rd;
    @Test
    public void UT_Division_p() {
        assertThat("Division does not work!", (int)calculator.Division(a, b), is(rd));
    }

}